import requests as rq
import ssl
import sqlite3
from bs4 import BeautifulSoup
from http.server import BaseHTTPRequestHandler, HTTPServer


def get_honest_score(product, title):
	headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

	def get_reviews(page=0):
		reviews_page = f'https://www.metacritic.com/{product}/{title}/user-reviews?page={page}'
	
		html = rq.get(reviews_page, headers=headers).text
		soup = BeautifulSoup(html, features="html.parser")

		div_reviews = soup.findAll('div', {'class': 'review'})

		reviews = []

		for div_review in div_reviews:
			rating = int(div_review.select('.metascore_w')[0].text)
			yes = int(div_review.select('.yes_count')[0].text)
			no = int(div_review.select('.total_count')[0].text) - yes

			reviews.append((rating, yes, no))

		try:
			pages = len(soup.select('ul.pages')[0].findAll('li'))
			if page < pages - 1:
				reviews.append(*get_reviews(page + 1))
		except:
			pass
		
		return reviews

	reviews = []

	# calculate honest score
	honest_num = 0	  # number of explicit (written) and implicit (agreed) reviews
	honest_total = 0  # accumulated rating

	for rating, yes, no in get_reviews():
		# the more votes the review has the more valuable it is
		value = max(0, yes - no) ** 1.5

		# don't take in account reviews with mostly negative responses
		if value > 0:
			honest_total += value * rating
			honest_num += value

	if honest_num > 0:
		hs = round(honest_total / honest_num, 1)
	else:
		hs = '?'

	return hs


def handle(path):
	conn = sqlite3.connect('db.sqlite')
	cur = conn.cursor()
	cur.execute('CREATE TABLE IF NOT EXISTS score(product TEXT, title TEXT, hs TEXT, PRIMARY KEY (product, title))')

	if '?' in path:
		request, params = path.split('?')
		_, product, title = request.split('/')
	else:	
		_, product, title = path.split('/')

	cached = cur.execute('SELECT hs FROM score WHERE product = ? AND title = ?', (product, title)).fetchone()

	if 'forced=true' in path:
		hs = get_honest_score(product, title)
		if cached:
			cur.execute('UPDATE score SET hs = ? WHERE product = ? AND title = ?', (hs, product, title))
		else:
			cur.execute('INSERT INTO score VALUES(?, ?, ?)', (product, title, hs))
		conn.commit()

	elif cached:
		hs = cached[0]

	else:
		hs = get_honest_score(product, title)
		if cached:
			cur.execute('UPDATE score SET hs = ? WHERE product = ? AND title = ?', (hs, product, title))
		else:
			cur.execute('INSERT INTO score VALUES(?, ?, ?)', (product, title, hs))
		conn.commit()

	return 200, hs


class Server(BaseHTTPRequestHandler):
	def do_GET(self):
		if 'favicon.ico' in self.path:
			# fuck this
			return

		code, response = handle(self.path)

		self.send_response(code)
		self.send_header('Access-Control-Allow-Origin', '*')
		self.end_headers()
		self.wfile.write(str(response).encode())


def serve():
	try:
		httpd = HTTPServer(('145.239.82.58', 1337), Server)
		httpd.socket = ssl.wrap_socket(httpd.socket, keyfile='key.key', certfile='cert.cert', server_side=True)
		httpd.serve_forever()
	except:
		httpd.socket.close()
		httpd.shutdown()


if __name__ == '__main__':
	serve()